# Boot Disk section #
variable "boot_disk" {
  type = map(any)
  default = {
    boot_disk_type     = "network-ssd"
    boot_disk_image_id = "fd8500b61gv8mj86b0ns"
    boot_disk_size     = 5
  }
  description = <<EOT
    boot_disk_type: Yandex Boot Disk Type: network-hdd, network-ssd, network-ssd-               nonreplicated, network-ssd-io-m3
    boot_disk_image_id: Yandex Boot Ubuntu image: ubuntu-20-04-lts-v20240101
    boot_disk_size: Yandex Boot Disk Size
EOT
}

# Instance Section #
variable "instances_default" {
  type = map(number)
  default = {
    core = 2
    memory = 2
    core_fraction = 100
  }
}

variable "instances" {
  type = any
  default = {
    web01 = {
      count = 1
      labels = {
        role = "nfs_client"
      }
      resources = {
        core          = 2
        memory        = 0.5
        core_fraction = 5
      }
      preemptible = true
    }
    files01 = {
      count = 1
      labels = {
        role = "nfs_server"
      }
      resources = {
        core          = 2
        memory        = 0.5
        core_fraction = 5
      }
      preemptible = true
    }
  }
  description = "Names of instaces to created"
}

variable "platform" {
  type        = string
  default     = "standard-v2"
  description = "Yandex platform id: standard-v1, standard-v2, standard-v3"
}

variable "zone" {
  type        = string
  default     = "ru-central1-a"
  description = "Yandex Region zone"
}

variable "ssh-key" {
  type = map(string)
  default = {
    user     = "ubuntu"
    key-file = "~/.ssh/yc-key.pub"
  }
  description = "ssh key and user name for instances: "
}
