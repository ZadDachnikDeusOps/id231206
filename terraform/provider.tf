terraform {
  required_providers {
    yandex = {
      source = "yandex-cloud/yandex"
    }
  }
  required_version = ">= 0.18"

  backend "s3" {
    endpoints = {
      s3       = "https://storage.yandexcloud.net"
      dynamodb = "https://docapi.serverless.yandexcloud.net/ru-central1/b1g3alqbv5l0h9i5to06/etnh4f89nhtqr94sd6v3"
    }
    bucket         = "s3-tfstate"
    region         = "ru-central1"
    key            = "tests/id231206.tfstate"
    dynamodb_table = "tf-lock-state"

    skip_region_validation      = true
    skip_credentials_validation = true
    skip_requesting_account_id  = true
    skip_s3_checksum            = true

  }
}

provider "yandex" {
  zone = var.zone
}
