output "vm_names" {
  value = [for key, val in yandex_compute_instance.vm : key]
}

output "external_ip_address_vm" {
  value = [for key, val in yandex_compute_instance.vm : join("", val.network_interface[*].nat_ip_address)]
}

resource "local_file" "inventory" {
  content = <<EOF
[all]
${join("", [for key, val in yandex_compute_instance.vm : "${val.name} ansible_host=${join("", val.network_interface.*.nat_ip_address)} ansible_user=${split(":",val.metadata.ssh-keys)[0]}\n" ])}

${join("", [ for key, val in yandex_compute_instance.vm : "[${val.labels.role}]\n${val.name}\n" ])}
EOF
  filename = "../inventory.ini"
}
