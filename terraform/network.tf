resource "yandex_vpc_network" "network" {
}

resource "yandex_vpc_subnet" "subnet" {
  zone           = var.zone
  network_id     = yandex_vpc_network.network.id
  v4_cidr_blocks = ["10.128.0.0/16"]
}
