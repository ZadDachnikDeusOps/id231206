resource "yandex_compute_instance" "vm" {
  for_each = var.instances

  name        = each.key
  platform_id = var.platform
  zone        = var.zone
  hostname    = "${each.key}."
  
  labels = each.value["labels"] != null ? each.value["labels"] : {}
  
  resources {
    cores         = lookup(each.value["resources"], "cores", var.instances_default.core)
    memory        = lookup(each.value["resources"], "memory", var.instances_default.memory)
    core_fraction = lookup(each.value["resources"], "core_fraction", var.instances_default.core_fraction)
  }

  scheduling_policy {
    preemptible = lookup(each.value, "preemptible", false)
  }

  boot_disk {
    initialize_params {
      image_id = var.boot_disk.boot_disk_image_id
      size     = var.boot_disk.boot_disk_size
      type     = var.boot_disk.boot_disk_type
    }
  }

  network_interface {
    subnet_id = yandex_vpc_subnet.subnet.id
    nat       = true
  }

  metadata = {
    ssh-keys = "${lookup(each.value, "ssh-key.user", var.ssh-key.user)}:${file("${lookup(each.value, "ssh-key.key-file", var.ssh-key.key-file)}")}"
  }

  lifecycle {
    ignore_changes = [
      allow_stopping_for_update,
    ]
  }
}
